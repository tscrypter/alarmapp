package com.grouptwo.alarmapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import java.util.Random;

/**
 * Math Activity which will prompt the user to answer a simple addition question before launching
 * the weather activity.
 * @author Cory Bradford, Tung Le
 * @version 1.0
 */
public class MathActivity extends AppCompatActivity {
    private int mathResult;
    private TextView mathQuestion;
    private EditText userInput;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_math);
        mathQuestion = findViewById(R.id.math_instruction);
        userInput = findViewById(R.id.userInput);

        generateQuestion();
    }

    @SuppressLint("SetTextI18n")
    private void generateQuestion() {
        Random rand = new Random();
        int firstNumber = rand.nextInt(10);
        int secondNumber = rand.nextInt(10);
        mathResult = firstNumber + secondNumber;
        mathQuestion.setText("What is the sum of " + firstNumber + " + " + secondNumber + " ?");
    }

    public void getUserResponse(View view) {
        int userAnswer = Integer.parseInt(userInput.getText().toString());
        if (userAnswer == mathResult) {
            Intent intent = new Intent(this, WeatherActivity.class);
            startActivity(intent);
        } else {
            Context context = getApplicationContext();
            CharSequence text = "That answer is incorrect - Please try again.";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
    }
}