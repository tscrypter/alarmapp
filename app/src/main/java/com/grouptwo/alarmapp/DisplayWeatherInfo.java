package com.grouptwo.alarmapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class DisplayWeatherInfo extends AppCompatActivity {

    TextView currentTemperature;
    TextView cityName;
    TextView description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_weather_info);

        currentTemperature = findViewById(R.id.currentTemperature);
        cityName = findViewById(R.id.cityName);
        description = findViewById(R.id.description);

        Intent intent = getIntent();
        currentTemperature.setText(intent.getStringExtra("temperature"));
        cityName.setText(intent.getStringExtra("city"));
        description.setText(intent.getStringExtra("description"));

    }

}