package com.grouptwo.alarmapp.DAOs;

import androidx.lifecycle.LiveData;
import androidx.room.*;
import com.grouptwo.alarmapp.entities.Alarm;

import java.util.List;

@Dao
public interface AlarmDAO {

    @Insert
    void insert(Alarm alarm);

    @Delete
    void delete(Alarm alarm);

    @Update
    void update(Alarm alarm);

    @Query("DELETE FROM alarm_table")
    void deleteAll();

    @Query("SELECT * from alarm_table ORDER BY id ASC")
    LiveData<List<Alarm>> getAllAlarms();
}
