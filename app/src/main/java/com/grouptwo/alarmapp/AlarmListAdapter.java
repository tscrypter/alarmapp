package com.grouptwo.alarmapp;

import android.content.Context;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.grouptwo.alarmapp.entities.Alarm;

import java.util.List;

public class AlarmListAdapter extends RecyclerView.Adapter<AlarmListAdapter.AlarmViewHolder> {

    private java.text.DateFormat timeFormat;
    private final LayoutInflater inflater;
    private List<Alarm> alarms; // cached copy of alarms

    public AlarmListAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
        this.timeFormat = DateFormat.getTimeFormat(context);
    }

    @NonNull
    @Override
    public AlarmViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.recyclerview_item, parent, false);
        return new AlarmViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AlarmViewHolder holder, int position) {
        if (alarms != null) {
            Alarm current = alarms.get(position);
            holder.alarmItemView.setText(timeFormat.format(current.getAlarmTime()));
        } else {
            holder.alarmItemView.setText("No Alarm");
        }
    }

    void setAlarms(List<Alarm> alarms) {
        this.alarms = alarms;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if(alarms != null) {
            return alarms.size();
        }
        return 0;
    }

    class AlarmViewHolder extends RecyclerView.ViewHolder {
        private final TextView alarmItemView;

        private AlarmViewHolder(View itemView) {
            super(itemView);
            alarmItemView = itemView.findViewById(R.id.textView);
        }
    }
}
