package com.grouptwo.alarmapp;

import android.app.Activity;
import android.content.Intent;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import com.squareup.seismic.ShakeDetector;

import static com.squareup.seismic.ShakeDetector.SENSITIVITY_MEDIUM;

public class ShakeActivity extends Activity implements ShakeDetector.Listener {
    private static final String LOG_TAG = ShakeActivity.class.getSimpleName();
    private int shakeCount = 5;
    private TextView shakeCountDisplay;
    private SensorManager sm;
    private ShakeDetector sd;
    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shake);

        shakeCountDisplay = findViewById(R.id.shake_instruction);

        shakeCountDisplay.setText(shakeCount > 1 ? "Please shake " + shakeCount + " times!"
                                    : "Please shake " + shakeCount + " time!");

        sm = (SensorManager) getSystemService(SENSOR_SERVICE);
        sd = new ShakeDetector(this);
        sd.setSensitivity(SENSITIVITY_MEDIUM);
        sd.start(sm);
    }

    @Override public void hearShake() {
        Log.d(LOG_TAG,"Device is shaking!");
        --shakeCount;
        if (shakeCount == 0) {
            finish();
            sd.stop();
            Intent intent = new Intent(this, WeatherActivity.class);
            startActivity(intent);
        }
        shakeCountDisplay.setText(shakeCount > 1 ? "Please shake " + shakeCount + " times!"
                : "Please shake " + shakeCount + " time!");
    }
}