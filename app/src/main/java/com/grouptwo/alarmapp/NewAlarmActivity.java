package com.grouptwo.alarmapp;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import java.util.Calendar;
import java.util.Date;

public class NewAlarmActivity extends AppCompatActivity {
    public static final String DATE_TIME = "com.grouptwo.alarmapp.DATE_TIME";
    public static final String SUNDAY_ENABLED = "com.grouptwo.alarmapp.SUNDAY_ENABLED";
    public static final String MONDAY_ENABLED = "com.grouptwo.alarmapp.MONDAY_ENABLED";
    public static final String TUESDAY_ENABLED = "com.grouptwo.alarmapp.TUESDAY_ENABLED";
    public static final String WEDNESDAY_ENABLED = "com.grouptwo.alarmapp.WEDNESDAY_ENABLED";
    public static final String THURSDAY_ENABLED = "com.grouptwo.alarmapp.THURSDAY_ENABLED";
    public static final String FRIDAY_ENABLED = "com.grouptwo.alarmapp.FRIDAY_ENABLED";
    public static final String SATURDAY_ENABLED = "com.grouptwo.alarmapp.SATURDAY_ENABLED";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_alarm);
        final Button saveButton = findViewById(R.id.saveButton);
        final TimePicker timePicker = findViewById(R.id.timepicker);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent replyIntent = new Intent();
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                c.set(mYear, mMonth, mDay, timePicker.getHour(), timePicker.getMinute());
                long time = c.getTime().getTime();
                replyIntent.putExtra(DATE_TIME, time);
                setResult(RESULT_OK, replyIntent);
                finish();
            }
        });
    }
}