package com.grouptwo.alarmapp;

import android.app.Application;
import android.os.AsyncTask;
import androidx.lifecycle.LiveData;
import com.grouptwo.alarmapp.DAOs.AlarmDAO;
import com.grouptwo.alarmapp.DAOs.AlarmRoomDatabase;
import com.grouptwo.alarmapp.entities.Alarm;

import java.util.List;

public class AlarmRepository {

    private AlarmDAO alarmDAO;
    private LiveData<List<Alarm>> allAlarms;

    public AlarmRepository(Application application) {
        AlarmRoomDatabase db = AlarmRoomDatabase.getDatabase(application);
        alarmDAO = db.alarmDAO();
        allAlarms = alarmDAO.getAllAlarms();
    }

    public LiveData<List<Alarm>> getAllAlarms() {
        return allAlarms;
    }

    public void insert(Alarm alarm) {
        new insertAsyncTask(alarmDAO).execute(alarm);
    }

    private static class insertAsyncTask extends AsyncTask<Alarm, Void, Void> {

        private AlarmDAO asyncAlarmDAO;

        insertAsyncTask(AlarmDAO dao) {
            asyncAlarmDAO = dao;
        }

        @Override
        protected Void doInBackground(Alarm... alarms) {
            asyncAlarmDAO.insert(alarms[0]);
            return null;
        }
    }
}
