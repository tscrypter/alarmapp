package com.grouptwo.alarmapp.entities;

import androidx.room.*;

import java.util.Date;


@Entity(tableName = "alarm_table")
@TypeConverters(value = {Converters.class})
public class Alarm {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private Integer id;
    @ColumnInfo(name = "time")

    private Date alarmTime;

    public Alarm() {
    }

    public Alarm(Integer id, Date alarmTime) {
        this.id = id;
        this.alarmTime = alarmTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getAlarmTime() {
        return alarmTime;
    }

    public void setAlarmTime(Date alarmTime) {
        this.alarmTime = alarmTime;
    }
}
