package com.grouptwo.alarmapp.ViewModels;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import com.grouptwo.alarmapp.AlarmRepository;
import com.grouptwo.alarmapp.entities.Alarm;

import java.util.List;

public class AlarmViewModel extends AndroidViewModel {

    private AlarmRepository alarmRepository;

    private LiveData<List<Alarm>> allAlarms;

    public AlarmViewModel(@NonNull Application application) {
        super(application);
        alarmRepository = new AlarmRepository(application);
        allAlarms = alarmRepository.getAllAlarms();
    }

    public LiveData<List<Alarm>> getAllAlarms() {
        return allAlarms;
    }

    public void insert(Alarm alarm) { alarmRepository.insert(alarm);}
}
